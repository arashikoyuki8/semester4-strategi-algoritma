package com.UAS;

import java.util.ArrayList;
import java.util.List;

public class Nomor3 {
    public static void main(String[] args) {
        int[] warungLocations = {10, 25, 30, 40, 50, 75, 80, 110, 130};
        int tripDistance = 140;
        int restInterval = 30;

        List<Integer> optimalStops = findOptimalStops(warungLocations, tripDistance, restInterval);

        System.out.println("Optimal Stops: " + optimalStops);
    }

    public static List<Integer> findOptimalStops(int[] warungLocations, int tripDistance, int restInterval) {
        List<Integer> optimalStops = new ArrayList<>();
        int currentPosition = 0;

        while (currentPosition < tripDistance) {
            int nextStop = findNextStop(currentPosition, warungLocations, restInterval);
            if (nextStop == currentPosition) {
                // No valid stop found, break the loop
                break;
            }
            optimalStops.add(nextStop);
            currentPosition = nextStop;
        }

        return optimalStops;
    }

    public static int findNextStop(int currentPosition, int[] warungLocations, int restInterval) {
        int nextStop = currentPosition + restInterval;

        for (int i = warungLocations.length - 1; i >= 0; i--) {
            if (warungLocations[i] <= nextStop) {
                nextStop = warungLocations[i];
                break;
            }
        }

        return nextStop;
    }
}
