# Implementasi graf
graph = {
    'A': [('B', 3), ('C', 2)],
    'B': [('D', 3), ('E', 5)],
    'C': [('B', 3), ('D', 6), ('E', 4)],
    'D': [('E', 2)],
    'E': []
}

def dfs(graph, start, goal):
    stack = [(start, 0, [start])]  # Tupel berisi (node, cost, path)
    visited = set()

    while stack:
        node, cost, path = stack.pop()

        if node == goal:
            return path, cost

        if node not in visited:
            visited.add(node)
            for neighbor, edge_cost in graph[node]:
                if neighbor not in visited:
                    new_cost = cost + edge_cost
                    new_path = path + [neighbor]
                    stack.append((neighbor, new_cost, new_path))

    return None

# Panggil fungsi DFS
start_node = 'A'
goal_node = 'E'
solution = dfs(graph, start_node, goal_node)

if solution:
    path, cost = solution
    print("Solusi optimal:", path)
    print("Total cost:", cost)
else:
    print("Tidak ada jalur yang menghubungkan", start_node, "dan", goal_node)
